import api from "../api-base";

export async function fetcher(url: string) {
  try {
    const response = await api.get(url);
    return await response.data;
  } catch (e) {
    console.log(e);
  }
}

export function getPageNum(currPage: number) {
  return currPage % 10 === 0 ? (currPage % 10) + 1 : currPage % 10;
}

export async function getAllHeroesId() {
  try {
    const heroes = await api.get("/heroes/");
    let ids = [];
    for (let i = 1; i <= heroes.data.count + 1; i++) {
      ids.push({
        params: {
          id: i.toString(),
        },
      });
    }
    return ids;
  } catch (e) {
    console.log(e);
  }
}

export async function getPagesCount() {
  try {
    const heroes = await api.get("/heroes/");
    return heroes.data.count;
  } catch (e) {
    console.log(e);
  }
}

export async function getHeroData(id: string) {
  try {
    const hero = await api.get(`/heroes/?id=${id}/`);
    return {
      id,
      ...hero.data,
    };
  } catch (e) {
    console.log(e);
  }
}
