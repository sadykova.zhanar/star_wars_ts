import "../styles/globals.css";
import { AppProps } from "next/app";
import Toolbar from "../components/toolbar";

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Toolbar />
      <Component {...pageProps} />
    </>
  );
}

export default App;
