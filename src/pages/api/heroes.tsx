import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

export default async function heroes(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const {
        query: { id, page },
      } = req;

      let path = "https://swapi.dev/api/people";
      if (page) {
        path += `?page=${page}`;
      }
      if (id) {
        path += `/${id}`;
      }
      const response = await axios.get(path);
      res.status(200).json(response.data);
    } catch (e) {
      res.status(400).json({ text: e });
    }
  }
}
