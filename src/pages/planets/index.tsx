import React, { useState, useEffect } from "react";
import useSWR from "swr";
import { GetStaticProps, GetServerSideProps } from "next";
import PlanetList from "../../components/planetsList";
import Loader from "../../components/loader";
import Layout from "../../components/layout";
import { fetcher, getPageNum, getPagesCount } from "../../lib/planets";
import styles from "../../styles/Home.module.css";
import { useRouter } from "next/router";

export interface PlanetsResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<PlanetType>;
}

export interface PlanetType {
  name: string;
  rotation_period: string;
  orbital_period: string;
  diameter: string;
  climate: string;
  gravity: string;
  terrain: string;
  surface_water: string;
  population: string;
  residents: string[];
  films: string[];
  created: string;
  edited: string;
  url: string;
}

export const getStaticProps: GetStaticProps = async () => {
  const pagesCount = await getPagesCount();
  return {
    props: {
      pagesCount,
    },
  };
};



export default function Planets({ pagesCount }: { pagesCount: number }) {
  const [currPage, setCurrPage] = useState(1);
  const [indexOfPageRange, setindexOfPageRange] = useState(0);
  const router = useRouter();
  const { page, index } = router.query;

  useEffect(() => {
    if (page) {
      setCurrPage(parseInt(page.toString()));
    }
    if (index) {
      setindexOfPageRange(parseInt(index.toString()));
    }
  }, []);
  const { data: planets, error } = useSWR<PlanetsResponse>(
    `/planets/?page=${getPageNum(currPage)}`,
    fetcher
  );
  
  if (error) return <div>failed to load</div>;

  return (
    <Layout
      info
      currPage={currPage}
      indexOfPageRange={indexOfPageRange}
      changePage={(page) => {
        setCurrPage(page);
      }}
      changePageRange={(index) => {
        setindexOfPageRange(index);
      }}
      pageCount={pagesCount}
    >
      {!planets ? (
        <Loader />
      ) : (
        <div className={styles.description}>
          Planets
          <PlanetList planets={planets.results} />
        </div>
      )}
    </Layout>
  );
}
