import { getAllPlanetsId, getPlanetData } from "../../lib/planets";
import { PlanetType } from ".";
import { Card } from "antd";
import { GetStaticProps, GetStaticPaths } from "next";
import Layout from "../../components/layout";

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getAllPlanetsId();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const planetData = await getPlanetData(params.id as string);
  return {
    props: {
      planetData,
    },
  };
};

export default function Planet({ planetData }: { planetData: PlanetType }) {
  return (
    <Layout>
      <Card title={planetData.name}>
        <p>Climate: {planetData.climate}</p>
        <p>Diameter: {planetData.diameter}</p>
        <p>Gravity: {planetData.gravity}</p>
        <p>Population: {planetData.population}</p>
        <p>Orbital period: {planetData.orbital_period}</p>
      </Card>
    </Layout>
  );
}
