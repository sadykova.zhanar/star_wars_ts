import Head from "next/head";
import styles from "./layout.module.css";
import Pagination from "./pagination";

export default function Layout({
  children,
  info,
  currPage,
  indexOfPageRange,
  changePage,
  changePageRange,
  pageCount,
}: {
  children: React.ReactNode;
  info?: boolean;
  currPage?: number;
  indexOfPageRange?: number;
  changePage?: (page: number) => any;
  changePageRange?: (index: number) => any;
  pageCount?: number;
}) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Star wars</title>
      </Head>
      <main className={styles.main}>{children}</main>
      {info ? (
        <Pagination
          currPage={currPage}
          indexOfPageRange={indexOfPageRange}
          changePage={changePage}
          changePageRange={changePageRange}
          pageCount={pageCount}
        />
      ) : null}
    </div>
  );
}
