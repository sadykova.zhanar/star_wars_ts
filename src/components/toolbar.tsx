import React from "react";
import { Menu } from "antd";
import styles from "./layout.module.css";
import { useRouter } from "next/router";

export default function Toolbar() {
  const router = useRouter();
  const path = `/${router.pathname.split("/")[1]}`;
  return (
    <Menu
      onClick={(e: any) => {
        router.push(e.key);
      }}
      selectedKeys={[path]}
      mode="horizontal"
      theme="dark"
      className={styles.container}
    >
      <Menu.Item key="/">Home</Menu.Item>
      <Menu.Item key="/planets">Planets</Menu.Item>
      <Menu.Item key="/heroes">Heroes</Menu.Item>
    </Menu>
  );
}
