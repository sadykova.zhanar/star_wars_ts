import React from "react";
import { useRouter } from "next/router";
import type { HeroType } from "../pages/heroes";
import { List } from "antd";


interface AllHeroesProps {
  heroes: Array<HeroType>;
}

function HeroesList({ heroes }: AllHeroesProps) {
  const router = useRouter();
  const { page, index } = router.query;
  return (
    <List
      size="large"
      bordered
      dataSource={heroes}
      renderItem={(item) => (
        <List.Item>
          <a
            onClick={() => {
              router.push({
                pathname: `/heroes/${item.url.split("/")[5]}`,
                query: { page, index },
              });
            }}
          >
            {item.name}
          </a>
        </List.Item>
      )}
    />
  );
}
export default HeroesList;
