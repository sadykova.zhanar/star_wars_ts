import React from "react";
import { Button } from "antd";

interface PaginationItemProps {
  onClick: (event: any) => void;
  pageNumber: number | string;
  currPage: number;
}

function PaginationItem({
  onClick,
  pageNumber,
  currPage,
}: PaginationItemProps) {
  let selectedPage = false;
  if (pageNumber === currPage) selectedPage = true;
  return (
    <Button
      type="primary"
      shape="circle"
      onClick={onClick}
      disabled={selectedPage}
      style={
        pageNumber === "..."
          ? { marginLeft: "5px", marginRight: "5px" }
          : { marginLeft: "0", marginRight: "0" }
      }
    >
      {pageNumber}
    </Button>
  );
}
export default PaginationItem;
