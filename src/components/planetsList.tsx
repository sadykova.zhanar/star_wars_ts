import React from "react";
import { useRouter } from "next/router";
import type { PlanetType } from "../pages/planets";
import { List } from "antd";

interface AllPlanetsProps {
  planets: Array<PlanetType>;
}

function PlanetList({ planets }: AllPlanetsProps) {
  const router = useRouter();
  const { page, index } = router.query;
  return (
    <List
      size="large"
      bordered
      dataSource={planets}
      renderItem={(item) => (
        <List.Item>
          <a
            onClick={() => {
              router.push({
                pathname: `/planets/${item.url.split("/")[5]}`,
                query: { page, index },
              });
            }}
          >
            {item.name}
          </a>
        </List.Item>
      )}
    />
  );
}
export default PlanetList;
